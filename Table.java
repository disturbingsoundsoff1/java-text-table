
import java.util.ArrayList;

public class Table {
	ArrayList<ArrayList<String>> table;
	int cur;
	int maxRowLen;
	int curRowLen;

	public Table() {
		maxRowLen = 0;
		cur = -1;
		table = new ArrayList<>();
		endRow();
	}

	public void endRow() {
		table.add(new ArrayList<>());
		curRowLen = 0;
		cur++;
	}

	public <T> void add(T input) {
		String str = input.toString();
		curRowLen++;
		if (maxRowLen < curRowLen)
			maxRowLen = curRowLen;

		table.get(cur).add(str);
	}

	private int[] countTableSize() {
		int[] tableSize = new int[maxRowLen];
		for (int i = 0; i < maxRowLen; i++) {
			for (int j = 0; j < table.size(); j++) {
				if (table.get(j).get(i).length() > tableSize[i])
					tableSize[i] = table.get(j).get(i).length();
			}
		}
		return tableSize;
	}

	private String makeTable() {

		table.forEach((list) -> {
			while (list.size() < maxRowLen)
				list.add("");
		});

		int[] tableSize = countTableSize();

		StringBuilder separator = new StringBuilder("+");
		for (int i = 0; i < tableSize.length; i++) {
			for (int j = 0; j < tableSize[i]; j++)
				separator.append("-");
			separator.append("+");
		}

		StringBuilder output = new StringBuilder("");
		for (int i = 0; i < table.size(); i++) {
			output.append(separator.toString() + "\n");

			StringBuilder line = new StringBuilder("");
			line.append("|");

			for (int j = 0; j < table.get(0).size(); j++) {
				int len = tableSize[j] - table.get(i).get(j).length();
				String format = "";
				if (len == 0)
					format = "" + "%s";
				else
					format = "%" + len + "s";
				line.append(table.get(i).get(j));
				line.append(String.format(format, ""));
				line.append("|");
			}
			output.append(line + "\n");
		}
		output.append(separator + "\n");
		return output.toString();
	}

	public String toString() {
		return makeTable();
	}
}
